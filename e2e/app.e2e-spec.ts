import { MoviesAngularPage } from './app.po';

describe('movies-angular App', () => {
  let page: MoviesAngularPage;

  beforeEach(() => {
    page = new MoviesAngularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to mv!');
  });
});
