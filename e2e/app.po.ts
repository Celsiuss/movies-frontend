import { browser, by, element } from 'protractor';

export class MoviesAngularPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('mv-root h1')).getText();
  }
}
