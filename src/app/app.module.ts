import 'hammerjs';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {ResponsiveModule} from 'ng2-responsive';
import { SidenavComponent } from './sidenav/sidenav.component';
import { ListingComponent } from './listing/listing.component';
import {HttpModule} from '@angular/http';
import {ReactiveFormsModule} from '@angular/forms';
import {VideoService} from './video.service';
import { MovieComponent } from './movie/movie.component';
import { HomeComponent } from './home/home.component';
import {SidebarService} from './sidebar.service';
import {ConfigService} from './config.service';
import { LoginComponent } from './auth/login.component';
import { RegisterComponent } from './auth/register.component';
import { VideojsComponent } from './videojs/videojs.component';
import {AuthService} from './auth/auth.service';
import {SharedModule} from './shared.module';
import { SerieComponent } from './serie/serie.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidenavComponent,
    ListingComponent,
    MovieComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    VideojsComponent,
    SerieComponent
  ],
  entryComponents: [
    RegisterComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    ResponsiveModule,
    HttpModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [],
  providers: [VideoService, SidebarService, ConfigService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
