import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from './auth.service';
import {User} from './user.model';

@Component({
  selector: 'mv-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  signupForm: FormGroup;

  constructor(private authService: AuthService) { }

  onSubmit() {
    const user = new User(
      this.signupForm.value.email,
      this.signupForm.value.password,
      this.signupForm.value.username
    );
    this.authService.signup(user)
      .subscribe(
        data => console.log(data),
        error => {
          if (error.error.code === 11000) {
            this.signupForm.controls.email.setErrors({
              'duplicateEmail': true
            });
          }
        }
      );
  }

  ngOnInit() {
    this.signupForm = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.pattern(/^.{4,}$/)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.pattern(/^.{6,}$/)
      ]),
      /*confirmPassword: new FormControl('', [
        Validators.required
      ])*/
    });
  }

  get email(): any {
    return this.signupForm.get('email');
  }
  get username(): any {
    return this.signupForm.get('username');
  }
  get password(): any {
    return this.signupForm.get('password');
  }
  get confirmPassword(): any {
    return this.signupForm.get('confirmPassword');
  }

}
