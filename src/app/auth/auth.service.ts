import { Injectable } from '@angular/core';
import {User} from './user.model';
import {Headers, Http, Response} from '@angular/http';
import {ConfigService} from '../config.service';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthService {

  constructor(private http: Http,
              private config: ConfigService) { }

  signup(user: User) {
    const body = JSON.stringify(user);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post(this.config.config.userUrl, body, {headers: headers})
      .map((response: Response) => response.json())
      .catch((error: Response) => Observable.throw(error.json()));
  }

  login(user: User) {
    const body = JSON.stringify(user);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post(this.config.config.userUrl + 'login', body, {headers: headers})
      .map((response: Response) => response.json())
      .catch((error: Response) => Observable.throw(error.json()));
  }

  logout() {
    localStorage.clear();
  }

  isLoggedIn() {
    return localStorage.getItem('token') !== null;
  }

}
