import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {VideoService} from '../video.service';
import {AdminService} from './admin.service';

@Component({
  selector: 'mv-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  @ViewChild('sf') seriesForm;
  files = [];
  folders = [];
  currentFolder = [];
  selectedVideo = '';
  seriesArray: any = [];

  constructor(private videoService: VideoService,
              private adminService: AdminService) { }

  ngOnInit() {
    this.openFolder();
  }

  movieSubmit(form: NgForm) {
    console.log(form.value.videoSource);
    const data = {
      name: form.value.name,
      videoSource: this.selectedVideo,
      subtitleSource: form.value.subtitleSource,
      quality: form.value.quality
    };
    this.adminService.addVideo(data)
      .subscribe(
        res => console.log(res),
        error => console.log(error)
      );
    form.resetForm();
  }

  serieSubmit(form: NgForm) {
    const data = {
      name: form.value.sname,
      type: form.value.stype
    };
    let videosArray: any = [
      [
        {}
      ]
    ];
    for (const season of this.seriesArray) {
      let videos: any = [];
      for (const video of season.videos) {
        const videoNumber = Number(season.videos.indexOf(video)) + 1;
        const videoName =  this.seriesForm.value['s' + season.season + 'e' + (Number(season.videos.indexOf(video)) + 1)];
        videos.push({episode: videoNumber, title: videoName, path: video});
      }
      videosArray.push(videos);
    }
    console.log('videosArray');
    console.log(videosArray);

    const body = {
      name: form.value.name,
      quality: form.value.quality,
      cover: form.value.cover,
      videos: videosArray
    };

    this.adminService.addSerie(body)
      .subscribe(
        res => console.log(res),
        error => console.log(error)
      );
  }

  openFolder(folder?: string) {
    if (folder) {
      if (folder === 'uponelevel') {
        this.currentFolder.pop();
      } else {
        this.currentFolder.push(folder);
      }
    }
    this.adminService.getFiles(this.currentFolder.join('/'))
      .subscribe(
        files => {
          this.files = [];
          this.folders = [];
          for (const file of files) {
            if (file.endsWith('.mp4')
              || file.endsWith('.avi')
              || file.endsWith('.mkv')
              || file.endsWith('.ogg')) {
              this.files.push(file);
            } else {
              this.folders.push(file);
            }
          }

        },
        error => console.error(error)
      );
  }

  addVideo(video: string) {
    if (this.currentFolder.length !== 0) {
      this.selectedVideo = this.currentFolder.join('/') + '/' + video;
    } else {
      this.selectedVideo = video;
    }

  }

  addVideos() {
    this.adminService.getFiles(this.currentFolder.join('/'))
      .subscribe(
        files => {
          const season = this.seriesForm.value.season;
          const newFiles = [];
          for (const file of files) {
            newFiles.push(this.currentFolder.join('/') + '/' + file);
          }
          this.seriesArray.push({season: season, videos: newFiles});
          console.log(this.seriesArray);
        },
        error => console.error(error)
      );
  }

}
