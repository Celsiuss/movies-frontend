import {NgModule} from '@angular/core';
import { AdminComponent } from './admin.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared.module';
import {AdminService} from './admin.service';

@NgModule({
  declarations: [
    AdminComponent
  ],
  entryComponents: [

  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {path: '', component: AdminComponent}
    ])
  ],
  exports: [
    RouterModule
  ],
  providers: [AdminService],
  bootstrap: []
})
export class AdminModule { }
