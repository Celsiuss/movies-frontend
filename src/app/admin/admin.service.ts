import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {ConfigService} from '../config.service';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AdminService {

  constructor(private http: Http,
              private config: ConfigService) {}


  getFiles(folder?: string) {
    let url = this.config.config.adminUrl + 'files?t=' + localStorage.token;
    if (folder) {
      url += '&f=' + folder;
    }
    return this.http.get(url)
      .map((response: Response) => {
        return response.json().obj;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  addVideo(video) {
    const body = JSON.stringify(video);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post(this.config.config.adminUrl + '?t=' + localStorage.getItem('token'), body, {headers: headers})
      .map((response: Response) => response.json())
      .catch((error: Response) => Observable.throw(error.json()));
  }

  addSerie(video) {
    const body = JSON.stringify(video);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post(this.config.config.adminUrl + 'serie?t=' + localStorage.getItem('token'), body, {headers: headers})
      .map((response: Response) => response.json())
      .catch((error: Response) => Observable.throw(error.json()));
  }

}
