import {Component, OnInit} from '@angular/core';
import {SidebarService} from '../sidebar.service';
import {MdDialog} from '@angular/material';
import {RegisterComponent} from '../auth/register.component';
import {LoginComponent} from '../auth/login.component';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'mv-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private sidebarService: SidebarService,
              private dialog: MdDialog,
              private authService: AuthService) { }

  ngOnInit() {
  }

  onSidenavToggleClick() {
    this.sidebarService.toggle();
  }

  openLogin() {
    this.dialog.open(LoginComponent);
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

}
