import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {SidebarService} from './sidebar.service';
import {MdDialog, MdSidenav} from '@angular/material';
import {LoginComponent} from './auth/login.component';

@Component({
  selector: 'mv-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('sidenav') sidenav: MdSidenav;

  constructor(private sidebarService: SidebarService,
              private dialog: MdDialog) {}

  ngAfterViewInit () {
    this.sidebarService.sidenav = this.sidenav;
  }

}

