import { Injectable } from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {ConfigService} from './config.service';

@Injectable()
export class VideoService {
  private videos;

  constructor(private http: Http,
              private config: ConfigService) { }

  getVideos() {
    return this.http.get(this.config.config.videoUrl)
      .map((response: Response) => {
        return this.videos = response.json().obj;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  getVideo(video: string) {
    return this.http.get(this.config.config.videoUrl + video)
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  getCurrentVideos() {
    return this.videos;
  }

}
