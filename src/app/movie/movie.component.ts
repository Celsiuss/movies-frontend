import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {VideoService} from '../video.service';
import {ConfigService} from '../config.service';


declare let videojs: any;

@Component({
  selector: 'mv-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit, OnDestroy {
  videoId: string;
  video: any = {
    name: 'Loading...'
  };
  private videoJSplayer: any;
  path = this.config.config.videoUrl + 'movie/';
  // uid = Math.floor(Math.random() * 1000000000);

  metadata = {
    decription: '',
    rating: '',
    length: ''
  };

  passedVideo: any = {
    id: null,
    subtitleSource: null,
    path: null,
    // player: null
  };

  constructor(private route: ActivatedRoute,
              private videoService: VideoService,
              private config: ConfigService) { }

  ngOnInit() {
    this.videoId = this.route.snapshot.params.id;


    this.videoService.getVideo(this.videoId)
      .subscribe(video => {
        this.video = video.obj;
        const options = {
          width: screen.width * 0.8,
          plugins: {
            chromecast: {
              appId: 'APP-ID',
              metadata: {
                title: 'Title display on tech wrapper',
                subtitle: 'Synopsis display on tech wrapper',
              }
            }
          }
        };

        this.passedVideo = {
          id: video.obj._id,
          subtitleSource: this.config.config.baseUrl + 'subtitles/' + video.obj.subtitleSource,
          path: this.path + this.video._id + '?t=' + video.token,
          // player: this.videoJSplayer
        };

        setTimeout(() => {
          this.videoJSplayer = videojs(document.getElementById('video_' + video.obj._id), options, function() {
            this.aspectRatio('16:9');
            // this.play();
          } );
        }, 1);


      });

  }

  ngOnDestroy() {
    this.videoJSplayer.dispose();
  }


}
