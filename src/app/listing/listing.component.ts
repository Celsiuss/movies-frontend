import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mv-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {
  @Input('videos') videos;

  constructor() { }

  ngOnInit() {
  }

  showOrMovie(type: string) {
    return type === 'movie' ? 'movie' : 'show';
  }

}
