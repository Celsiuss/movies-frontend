import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {VideoService} from '../video.service';
import {ConfigService} from '../config.service';

declare let videojs: any;

@Component({
  selector: 'mv-serie',
  templateUrl: './serie.component.html',
  styleUrls: ['./serie.component.css']
})
export class SerieComponent implements OnInit, OnDestroy {
  videoId: string;
  video: any = {
    name: 'Loading...'
  };
  private videoJSplayer: any;
  path = this.config.config.videoUrl + 'serie/';
  // uid = Math.floor(Math.random() * 1000000000);

  metadata = {
    decription: '',
    rating: '',
    length: ''
  };

  passedVideo: any = {
    id: null,
    subtitleSource: null,
    path: null,
    // player: null
  };

  currentEpisode: any = {};
  currentSeason: any = '';

  constructor(private route: ActivatedRoute,
              private videoService: VideoService,
              private config: ConfigService) { }

  ngOnInit() {
    this.videoId = this.route.snapshot.params.id;

    const urlS = this.route.snapshot.params.season;
    const urlE = this.route.snapshot.params.episode - 1;


    this.videoService.getVideo(this.videoId)
      .subscribe(video => {
        this.video = video.obj;

        const options = {
          width: screen.width * 0.8,
          plugins: {
            chromecast: {
              appId: 'APP-ID',
              metadata: {
                title: 'Title display on tech wrapper',
                subtitle: 'Synopsis display on tech wrapper',
              }
            }
          }
        };

        this.currentEpisode = this.video.seasons[urlS][urlE][0];
        this.currentSeason = urlS;
        console.log(this.video.seasons[1][0][0]);

        this.passedVideo = {
          id: video.obj._id,
          subtitleSource: this.config.config.baseUrl + 'subtitles/' + video.obj.subtitleSource,
          path: this.path + this.video.name + '/' + urlS + '/' + urlE + '?t=' + video.token,
          // player: this.videoJSplayer
        };
        console.log('PATHHHHH');
        console.log(this.passedVideo.path);

        setTimeout(() => {
          this.videoJSplayer = videojs(document.getElementById('video_' + video.obj._id), options, function() {
            this.aspectRatio('16:9');
            // this.play();
          } );
        }, 1);


      });

  }

  ngOnDestroy() {
    this.videoJSplayer.dispose();
  }

}
