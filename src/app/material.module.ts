import { NgModule } from '@angular/core';
import {
  MdButtonModule, MdCardModule, MdDialogModule, MdInputModule, MdSidenavModule,
  MdToolbarModule
} from '@angular/material';

@NgModule({
  imports: [
    MdButtonModule,
    MdToolbarModule,
    MdSidenavModule,
    MdCardModule,
    MdDialogModule,
    MdInputModule
  ],
  exports: [
    MdButtonModule,
    MdToolbarModule,
    MdSidenavModule,
    MdCardModule,
    MdDialogModule,
    MdInputModule
  ]
})
export class MaterialModule { }
