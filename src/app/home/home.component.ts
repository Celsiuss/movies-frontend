import {Component, OnInit, ViewChild} from '@angular/core';
import {VideoService} from '../video.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'mv-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('sidenav') sidenav;
  videos = [];

  constructor(private videoService: VideoService) {}

  ngOnInit() {
    this.videoService.getVideos()
      .subscribe(
        (videos) => {
          this.videos = videos;
        }
      );
  }

  toggle() {
    this.sidenav.toggle();
  }

}
