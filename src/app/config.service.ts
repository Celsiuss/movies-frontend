import {Injectable, isDevMode} from '@angular/core';

@Injectable()
export class ConfigService {
  public readonly config = {
    baseUrl: '',
    videoUrl: '',
    userUrl: '',
    adminUrl: ''
  };

  constructor() {
    if (isDevMode()) {
      this.config.baseUrl = 'http://localhost:3000/';
      this.config.videoUrl = 'http://localhost:3000/videos/';
      this.config.userUrl = 'http://localhost:3000/users/';
      this.config.adminUrl = 'http://localhost:3000/admin/';
    } else {
      this.config.baseUrl = 'https://cdn.easymoviez.online/';
      this.config.videoUrl = 'https://cdn.easymoviez.online/videos/';
      this.config.userUrl = 'https://cdn.easymoviez.online/users/';
      this.config.adminUrl = 'https://cdn.easymoviez.online/admin/';
    }
  }


}
