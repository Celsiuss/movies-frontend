import {ElementRef, Injectable} from '@angular/core';
import {MdSidenav} from '@angular/material';

@Injectable()
export class SidebarService {
  sidenav: MdSidenav;

  constructor() { }

  toggle() {
    this.sidenav.toggle();
  }

}
