import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mv-videojs',
  templateUrl: './videojs.component.html',
  styleUrls: ['./videojs.component.scss']
})
export class VideojsComponent implements OnInit {
  @Input() video: any;

  constructor() { }

  ngOnInit() {
  }

}
